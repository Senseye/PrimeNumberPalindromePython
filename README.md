# Palindrome made from the product of two 5-digit prime numbers

##### Result
```
Palindrome 999949999
Prime numbers 30109 * 33211
Duration 1.13856482506
```

##### Languages:
* [JavaScript](https://gitlab.com/Senseye/PrimeNumberPalindromeJavaScript)
* [Golang](https://gitlab.com/Senseye/PrimeNumberPalindromeGolang)
* [C](https://gitlab.com/Senseye/PrimeNumberPalindromeC)
* [Rust](https://gitlab.com/Senseye/PrimeNumberPalindromeRust)
#!/usr/bin/python

import time, math

# Max possible result, all 10-digits palindromes % 11 == 0
MAX = 999999999


def get_primes(start, finish):
    variants = [True] * finish

    limit = int(math.ceil(math.sqrt(finish)))

    for i in xrange(2, limit):
        if variants[i]:
            for j in xrange(i * i, finish, i):
                variants[j] = False

    result = []
    for i in xrange(start, finish):
        if variants[i]:
            result.append(i)

    return result


def is_palindrome(value):
    digits = [0, 0, 0, 0, 0, 0, 0, 0, 0]

    i = 0
    while value > 0:
        digits[i] = value % 10

        value //= 10

        i += 1

    limit = i >> 1
    end = i - 1

    for j in xrange(0, limit + 1):
        if digits[j] != digits[end - j]:
            return False

    return True


def find():
    primes = get_primes(10000, 99999)

    size = len(primes)

    result = (0, 0, 0)

    for i in xrange(0, size):
        for j in xrange(i + 1, size):
            left = primes[i]
            right = primes[j]

            possible = left * right

            if possible > MAX:
                break

            if possible > result[0] and is_palindrome(possible):
                result = (possible, left, right)

    return result


start_time = time.time()

result = find()

end_time = time.time()

print("Palindrome %d" % result[0])
print("Prime numbers %d * %d" % (result[1], result[2]))
print("Duration %s" % (end_time - start_time))
